#include <iostream>

#include "Demo.h"

using namespace std;

Demo invest;

int main(){
	cout<<"How much is your initial investment? ";
	int amount;
	cin>>amount;
	cin.clear();
	invest.setVal(amount);
	cout<<"Your initial investment is: $"<<invest.getVal()<<endl;
	cout<<"How much is your monthly deposit? ";
	cin>>amount;
	invest.setMonthDeposit(amount);
	cout<<"You set your monthly deposits to the amount: $"<<invest.getMonthDeposit()<<endl;
	cout<<"What is your annual rate? ";
	float myRate;
	cin>>myRate;
	invest.setAnnualRate(myRate);
	invest.makeMillion();
}
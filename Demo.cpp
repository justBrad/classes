#include "Demo.h"
#include <iostream>

using namespace std;

Demo::Demo(){
	value = 0;
	mthDeposit = 0;
}

void Demo::setVal(long double val ){
	value = val;
}

long double Demo::getVal(){
	return value;
}

void Demo::setMonthDeposit(long double val){
	mthDeposit = val;
}

long double Demo::getMonthDeposit(){
	return mthDeposit;
}

void Demo::setAnnualRate(float rate){
	interest_rate = rate;
}

void Demo::makeMillion(){
	cout<<fixed;
	cout.precision(2);
	int numMth = 1, numYear = 0;
	cin.ignore();
	while (value < 1000000){
		value+=mthDeposit;
		numMth++;
		cout<<"Year: "<<numYear<<" - "<<"Month: "<<numMth<<"\t$"<<value<<endl;
		if (numMth == 12){
			value += (value * interest_rate / 100);
			numYear++;
			numMth=0;
		}
	}
	cout<<"It will take "<<numYear<<" years and "<<numMth<<" months to earn $"<<value<<endl;
	cout<<"with monthly deposits of $"<<getMonthDeposit()<<" and in interest rate of "<<interest_rate<<"%"<<endl;
}
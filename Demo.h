class Demo{
	private:  //properties
		long double value;
		long double mthDeposit;
		float interest_rate; 
	public:  //methods
		Demo(); //default constructor
		void setVal(long double val);
		long double getVal();
		void setMonthDeposit(long double val);
		long double	getMonthDeposit();
		void setAnnualRate(float rate);
		void makeMillion();
 };